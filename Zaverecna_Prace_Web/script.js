//Scroll event
const entryElement = document.querySelector("#entry-element");
const fibonacciBox = document.querySelector("#fibonacci-scrollbar");

const navElement = document.querySelector("nav");
const navbarLogo = navElement.querySelector("#navbar-logo");
const navbarLinksBox = navElement.querySelector("#nav-right-part");

const scrollIndicator = fibonacciBox.querySelector(".progress-bar");
const fibonacciNumbers = fibonacciBox.querySelectorAll("p span");

const loaderBox = document.querySelector("#loader");
const loaderContent = loaderBox.querySelector("#loader-content");

const galleryImages = document.querySelectorAll("#gallery img");

//Toggle navbar
const toggleNavButton = document.querySelector("#nav-toggle-button");

//Display album
const albumsElements = document.querySelectorAll(".play-album");

document.addEventListener("scroll", () => {
    const entryElementSize = entryElement.getBoundingClientRect();

    const winScroll = document.body.scrollTop || document.documentElement.scrollTop;
    const height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    scrollIndicator.style.height = `${(winScroll / height) * 100}%`;

    if(window.scrollY >= entryElementSize.height) {
        navElement.classList.add("active");
        fibonacciBox.classList.add("active");
    }

    else {
        navElement.classList.remove("active");
        fibonacciBox.classList.remove("active");
    }
});

//Scroll button
const scrollButton = document.querySelector("#entry-scroll-button");
scrollButton.addEventListener("click", () => {
    const entryElementSize = entryElement.getBoundingClientRect();
    window.scroll({
        top: entryElementSize.height, 
        left: 0, 
        behavior: 'smooth'
    });
});

//Navbar buttons
const navbarButtons = document.querySelectorAll("nav button[data-target-element]");
navbarButtons.forEach(element => {
    element.addEventListener("click", () => {
        const targetElement = document.querySelector(`#${element.dataset.targetElement}`);
        targetElement.scrollIntoView({ behavior: "smooth", block: "start", inline: "nearest" });
        navbarLinksBox.classList.remove("active");
        toggleNavButton.classList.remove("active");
    });
});

//Navbar logo event
navbarLogo.addEventListener("click", () => {
    window.scroll({
        top: 0, 
        left: 0, 
        behavior: 'smooth'
    });
});

//Toggle navbar

toggleNavButton.addEventListener("click", () => {
    if(navbarLinksBox.classList.contains("active")) {
        navbarLinksBox.classList.remove("active");
        toggleNavButton.classList.remove("active");
    }

    else {
        navbarLinksBox.classList.add("active");
        toggleNavButton.classList.add("active");
    }
});

//Display wide image
for(const galleryImage of galleryImages) {
    galleryImage.addEventListener("click", () => {
        document.body.style.overflow = "hidden";

        const modalBox = document.createElement("div");
        modalBox.classList.add("modal-box");

        modalBox.addEventListener("click", (e) => {
            if(e.target == e.currentTarget) {
                document.body.style.overflow = null;
                modalBox.remove();
            }
        });

        const modalImage = document.createElement("img");
        modalImage.src = galleryImage.src;
        modalBox.appendChild(modalImage);

        const closeButton = document.createElement("button");
        closeButton.classList.add("close-button");
        modalBox.appendChild(closeButton);

        closeButton.addEventListener("click", () => {
            document.body.style.overflow = null;
            modalBox.remove();
        });

        document.body.appendChild(modalBox);
    });
}

//Play album
for(const albumElement of albumsElements) {
    albumElement.addEventListener("click", () => {
        console.log(albumElement.dataset.targetAlbum);

        document.body.style.overflow = "hidden";

        const modalBox = document.createElement("div");
        modalBox.classList.add("modal-box");

        modalBox.addEventListener("click", (e) => {
            if(e.target == e.currentTarget) {
                document.body.style.overflow = null;
                modalBox.remove();
            }
        });

        const modalIframe = document.createElement("iframe");
        modalIframe.setAttribute("allow", "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture");
        modalIframe.setAttribute("allowfullscreen", true);
        modalIframe.src = albumElement.dataset.targetAlbum;
        modalBox.appendChild(modalIframe);

        const closeButton = document.createElement("button");
        closeButton.classList.add("close-button");
        modalBox.appendChild(closeButton);

        closeButton.addEventListener("click", () => {
            document.body.style.overflow = null;
            modalBox.remove();
        });

        document.body.appendChild(modalBox);
    });
}


function onload() {
    const entryElementSize = entryElement.getBoundingClientRect();
    if(window.scrollY >= entryElementSize.height) {
        navElement.classList.add("active");
    }

    else {
        navElement.classList.remove("active");
    }

    const fetchImage = async () => {
        const response = await fetch("./assets/Images/BACKGROUND.png");
        const imageBlob = await response.blob();
        entryElement.style.backgroundImage = `url(${URL.createObjectURL(imageBlob)})`;

        loaderBox.addEventListener("transitionend", () => {
            loaderBox.remove();
        });
        loaderBox.style.opacity = 0;
    }

    fetchImage();
}

onload();