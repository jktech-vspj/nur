﻿namespace music_player
{
    partial class playerForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(playerForm));
            menuStrip1 = new MenuStrip();
            mediaToolStripMenuItem = new ToolStripMenuItem();
            openDirectoryStripMenuItem = new ToolStripMenuItem();
            openFileStripMenuItem = new ToolStripMenuItem();
            openFilesStripMenuItem = new ToolStripMenuItem();
            toolStripSeparator1 = new ToolStripSeparator();
            clearQueueStripMenuItem = new ToolStripMenuItem();
            removeFromPlaylistStripMenuItem = new ToolStripMenuItem();
            toolStripSeparator2 = new ToolStripSeparator();
            exitStripMenuItem = new ToolStripMenuItem();
            pomocToolStripMenuItem = new ToolStripMenuItem();
            helpStripMenuItem = new ToolStripMenuItem();
            aboutAppStripMenuItem = new ToolStripMenuItem();
            controlsPanel = new Panel();
            toggleMuteButton = new FontAwesome.Sharp.IconButton();
            progressBar = new Panel();
            progressIndicator = new Panel();
            songNameLabel = new Label();
            backwardButton = new FontAwesome.Sharp.IconButton();
            forwardButton = new FontAwesome.Sharp.IconButton();
            togglePlayButton = new FontAwesome.Sharp.IconButton();
            durationTimeLabel = new Label();
            currentTimeLabel = new Label();
            volumeTrack = new TrackBar();
            timer1 = new System.Windows.Forms.Timer(components);
            openFilesButton = new FontAwesome.Sharp.IconButton();
            musicListView = new ListView();
            id = new ColumnHeader();
            fileName = new ColumnHeader();
            duration = new ColumnHeader();
            current = new ColumnHeader();
            menuStrip1.SuspendLayout();
            controlsPanel.SuspendLayout();
            progressBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)volumeTrack).BeginInit();
            SuspendLayout();
            // 
            // menuStrip1
            // 
            menuStrip1.BackColor = Color.FromArgb(33, 37, 41);
            menuStrip1.ForeColor = Color.White;
            menuStrip1.Items.AddRange(new ToolStripItem[] { mediaToolStripMenuItem, pomocToolStripMenuItem });
            menuStrip1.Location = new Point(0, 0);
            menuStrip1.Name = "menuStrip1";
            menuStrip1.Size = new Size(984, 24);
            menuStrip1.TabIndex = 0;
            menuStrip1.Text = "menuStrip1";
            // 
            // mediaToolStripMenuItem
            // 
            mediaToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { openDirectoryStripMenuItem, openFileStripMenuItem, openFilesStripMenuItem, toolStripSeparator1, clearQueueStripMenuItem, removeFromPlaylistStripMenuItem, toolStripSeparator2, exitStripMenuItem });
            mediaToolStripMenuItem.ForeColor = Color.White;
            mediaToolStripMenuItem.ImageTransparentColor = Color.Transparent;
            mediaToolStripMenuItem.Name = "mediaToolStripMenuItem";
            mediaToolStripMenuItem.Size = new Size(52, 20);
            mediaToolStripMenuItem.Text = "Media";
            // 
            // openDirectoryStripMenuItem
            // 
            openDirectoryStripMenuItem.Name = "openDirectoryStripMenuItem";
            openDirectoryStripMenuItem.Size = new Size(172, 22);
            openDirectoryStripMenuItem.Text = "Otevřít složku";
            openDirectoryStripMenuItem.Click += openDirectoryStripMenuItem_Click;
            // 
            // openFileStripMenuItem
            // 
            openFileStripMenuItem.Name = "openFileStripMenuItem";
            openFileStripMenuItem.Size = new Size(172, 22);
            openFileStripMenuItem.Text = "Otevřít soubor";
            openFileStripMenuItem.Click += openFileStripMenuItem_Click;
            // 
            // openFilesStripMenuItem
            // 
            openFilesStripMenuItem.Name = "openFilesStripMenuItem";
            openFilesStripMenuItem.Size = new Size(172, 22);
            openFilesStripMenuItem.Text = "Otevřít soubory";
            openFilesStripMenuItem.Click += openFilesStripMenuItem_Click;
            // 
            // toolStripSeparator1
            // 
            toolStripSeparator1.Name = "toolStripSeparator1";
            toolStripSeparator1.Size = new Size(169, 6);
            // 
            // clearQueueStripMenuItem
            // 
            clearQueueStripMenuItem.Enabled = false;
            clearQueueStripMenuItem.Name = "clearQueueStripMenuItem";
            clearQueueStripMenuItem.Size = new Size(172, 22);
            clearQueueStripMenuItem.Text = "Vyčistit playlist";
            clearQueueStripMenuItem.Click += odebratSouboryToolStripMenuItem_Click;
            // 
            // removeFromPlaylistStripMenuItem
            // 
            removeFromPlaylistStripMenuItem.Enabled = false;
            removeFromPlaylistStripMenuItem.Name = "removeFromPlaylistStripMenuItem";
            removeFromPlaylistStripMenuItem.Size = new Size(172, 22);
            removeFromPlaylistStripMenuItem.Text = "Odebrat z playlistu";
            removeFromPlaylistStripMenuItem.Click += removeFromPlaylistStripMenuItem_Click;
            // 
            // toolStripSeparator2
            // 
            toolStripSeparator2.Name = "toolStripSeparator2";
            toolStripSeparator2.Size = new Size(169, 6);
            // 
            // exitStripMenuItem
            // 
            exitStripMenuItem.Name = "exitStripMenuItem";
            exitStripMenuItem.Size = new Size(172, 22);
            exitStripMenuItem.Text = "Odejít";
            exitStripMenuItem.Click += exitStripMenuItem_Click;
            // 
            // pomocToolStripMenuItem
            // 
            pomocToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { helpStripMenuItem, aboutAppStripMenuItem });
            pomocToolStripMenuItem.Name = "pomocToolStripMenuItem";
            pomocToolStripMenuItem.Size = new Size(73, 20);
            pomocToolStripMenuItem.Text = "Nápověda";
            // 
            // helpStripMenuItem
            // 
            helpStripMenuItem.Name = "helpStripMenuItem";
            helpStripMenuItem.Size = new Size(126, 22);
            helpStripMenuItem.Text = "Návod";
            helpStripMenuItem.Click += helpStripMenuItem_Click;
            // 
            // aboutAppStripMenuItem
            // 
            aboutAppStripMenuItem.Name = "aboutAppStripMenuItem";
            aboutAppStripMenuItem.Size = new Size(126, 22);
            aboutAppStripMenuItem.Text = "O aplikaci";
            aboutAppStripMenuItem.Click += aboutAppStripMenuItem_Click;
            // 
            // controlsPanel
            // 
            controlsPanel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            controlsPanel.BackColor = Color.FromArgb(33, 37, 41);
            controlsPanel.Controls.Add(toggleMuteButton);
            controlsPanel.Controls.Add(progressBar);
            controlsPanel.Controls.Add(songNameLabel);
            controlsPanel.Controls.Add(backwardButton);
            controlsPanel.Controls.Add(forwardButton);
            controlsPanel.Controls.Add(togglePlayButton);
            controlsPanel.Controls.Add(durationTimeLabel);
            controlsPanel.Controls.Add(currentTimeLabel);
            controlsPanel.Controls.Add(volumeTrack);
            controlsPanel.Location = new Point(0, 499);
            controlsPanel.Name = "controlsPanel";
            controlsPanel.Size = new Size(990, 82);
            controlsPanel.TabIndex = 1;
            // 
            // toggleMuteButton
            // 
            toggleMuteButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            toggleMuteButton.BackColor = Color.Transparent;
            toggleMuteButton.FlatAppearance.BorderSize = 0;
            toggleMuteButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
            toggleMuteButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
            toggleMuteButton.FlatStyle = FlatStyle.Flat;
            toggleMuteButton.IconChar = FontAwesome.Sharp.IconChar.VolumeHigh;
            toggleMuteButton.IconColor = Color.White;
            toggleMuteButton.IconFont = FontAwesome.Sharp.IconFont.Auto;
            toggleMuteButton.IconSize = 25;
            toggleMuteButton.Location = new Point(774, 34);
            toggleMuteButton.Name = "toggleMuteButton";
            toggleMuteButton.Size = new Size(30, 20);
            toggleMuteButton.TabIndex = 12;
            toggleMuteButton.TabStop = false;
            toggleMuteButton.UseVisualStyleBackColor = false;
            toggleMuteButton.Click += toggleMuteButton_Click;
            // 
            // progressBar
            // 
            progressBar.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            progressBar.BackColor = Color.FromArgb(51, 51, 51);
            progressBar.Controls.Add(progressIndicator);
            progressBar.Location = new Point(263, 64);
            progressBar.Margin = new Padding(0);
            progressBar.Name = "progressBar";
            progressBar.Padding = new Padding(3, 10, 3, 10);
            progressBar.Size = new Size(483, 5);
            progressBar.TabIndex = 11;
            progressBar.MouseDown += progressBar_MouseDown;
            // 
            // progressIndicator
            // 
            progressIndicator.BackColor = Color.White;
            progressIndicator.Enabled = false;
            progressIndicator.Location = new Point(0, 0);
            progressIndicator.Name = "progressIndicator";
            progressIndicator.Size = new Size(0, 5);
            progressIndicator.TabIndex = 0;
            // 
            // songNameLabel
            // 
            songNameLabel.AutoSize = true;
            songNameLabel.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            songNameLabel.ForeColor = Color.White;
            songNameLabel.Location = new Point(24, 34);
            songNameLabel.Name = "songNameLabel";
            songNameLabel.Size = new Size(111, 15);
            songNameLabel.TabIndex = 9;
            songNameLabel.Text = "Skladba nevybrána";
            // 
            // backwardButton
            // 
            backwardButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            backwardButton.BackColor = Color.Transparent;
            backwardButton.FlatAppearance.BorderSize = 0;
            backwardButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
            backwardButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
            backwardButton.FlatStyle = FlatStyle.Flat;
            backwardButton.IconChar = FontAwesome.Sharp.IconChar.BackwardStep;
            backwardButton.IconColor = Color.White;
            backwardButton.IconFont = FontAwesome.Sharp.IconFont.Auto;
            backwardButton.IconSize = 30;
            backwardButton.Location = new Point(444, 23);
            backwardButton.Name = "backwardButton";
            backwardButton.Size = new Size(30, 30);
            backwardButton.TabIndex = 8;
            backwardButton.TabStop = false;
            backwardButton.UseVisualStyleBackColor = false;
            backwardButton.Click += backwardButton_Click;
            // 
            // forwardButton
            // 
            forwardButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            forwardButton.BackColor = Color.Transparent;
            forwardButton.FlatAppearance.BorderSize = 0;
            forwardButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
            forwardButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
            forwardButton.FlatStyle = FlatStyle.Flat;
            forwardButton.IconChar = FontAwesome.Sharp.IconChar.ForwardStep;
            forwardButton.IconColor = Color.White;
            forwardButton.IconFont = FontAwesome.Sharp.IconFont.Auto;
            forwardButton.IconSize = 30;
            forwardButton.Location = new Point(540, 23);
            forwardButton.Name = "forwardButton";
            forwardButton.Size = new Size(30, 30);
            forwardButton.TabIndex = 7;
            forwardButton.TabStop = false;
            forwardButton.UseVisualStyleBackColor = false;
            forwardButton.Click += forwardButton_Click;
            // 
            // togglePlayButton
            // 
            togglePlayButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            togglePlayButton.BackColor = Color.Transparent;
            togglePlayButton.FlatAppearance.BorderSize = 0;
            togglePlayButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
            togglePlayButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
            togglePlayButton.FlatStyle = FlatStyle.Flat;
            togglePlayButton.ForeColor = Color.White;
            togglePlayButton.IconChar = FontAwesome.Sharp.IconChar.Play;
            togglePlayButton.IconColor = Color.White;
            togglePlayButton.IconFont = FontAwesome.Sharp.IconFont.Auto;
            togglePlayButton.IconSize = 40;
            togglePlayButton.Location = new Point(488, 18);
            togglePlayButton.Name = "togglePlayButton";
            togglePlayButton.Size = new Size(40, 40);
            togglePlayButton.TabIndex = 6;
            togglePlayButton.TabStop = false;
            togglePlayButton.UseVisualStyleBackColor = false;
            togglePlayButton.Click += togglePlayButton_Click;
            // 
            // durationTimeLabel
            // 
            durationTimeLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            durationTimeLabel.AutoSize = true;
            durationTimeLabel.ForeColor = Color.White;
            durationTimeLabel.Location = new Point(752, 58);
            durationTimeLabel.Name = "durationTimeLabel";
            durationTimeLabel.Size = new Size(34, 15);
            durationTimeLabel.TabIndex = 5;
            durationTimeLabel.Text = "00:00";
            // 
            // currentTimeLabel
            // 
            currentTimeLabel.AutoSize = true;
            currentTimeLabel.ForeColor = Color.White;
            currentTimeLabel.Location = new Point(223, 58);
            currentTimeLabel.Name = "currentTimeLabel";
            currentTimeLabel.Size = new Size(34, 15);
            currentTimeLabel.TabIndex = 4;
            currentTimeLabel.Text = "00:00";
            // 
            // volumeTrack
            // 
            volumeTrack.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            volumeTrack.LargeChange = 10;
            volumeTrack.Location = new Point(801, 34);
            volumeTrack.Maximum = 50;
            volumeTrack.Name = "volumeTrack";
            volumeTrack.Size = new Size(186, 45);
            volumeTrack.TabIndex = 3;
            volumeTrack.TabStop = false;
            volumeTrack.TickStyle = TickStyle.None;
            volumeTrack.Value = 50;
            volumeTrack.Scroll += volumeTrack_Scroll;
            // 
            // timer1
            // 
            timer1.Enabled = true;
            timer1.Tick += timer1_Tick;
            // 
            // openFilesButton
            // 
            openFilesButton.BackColor = Color.White;
            openFilesButton.Cursor = Cursors.Hand;
            openFilesButton.FlatAppearance.BorderSize = 0;
            openFilesButton.FlatAppearance.MouseDownBackColor = Color.White;
            openFilesButton.FlatAppearance.MouseOverBackColor = Color.White;
            openFilesButton.FlatStyle = FlatStyle.Flat;
            openFilesButton.Font = new Font("Segoe UI", 13F, FontStyle.Regular, GraphicsUnit.Point);
            openFilesButton.ForeColor = Color.FromArgb(33, 37, 41);
            openFilesButton.IconChar = FontAwesome.Sharp.IconChar.FolderOpen;
            openFilesButton.IconColor = Color.FromArgb(33, 37, 41);
            openFilesButton.IconFont = FontAwesome.Sharp.IconFont.Solid;
            openFilesButton.ImageAlign = ContentAlignment.MiddleLeft;
            openFilesButton.Location = new Point(408, 240);
            openFilesButton.Name = "openFilesButton";
            openFilesButton.Size = new Size(186, 73);
            openFilesButton.TabIndex = 4;
            openFilesButton.Text = "Otevřít soubory";
            openFilesButton.TextAlign = ContentAlignment.MiddleRight;
            openFilesButton.UseVisualStyleBackColor = false;
            openFilesButton.Click += openFilesButton_Click;
            // 
            // musicListView
            // 
            musicListView.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            musicListView.Columns.AddRange(new ColumnHeader[] { id, fileName, duration, current });
            musicListView.FullRowSelect = true;
            musicListView.GridLines = true;
            musicListView.HeaderStyle = ColumnHeaderStyle.Nonclickable;
            musicListView.Location = new Point(0, 24);
            musicListView.Name = "musicListView";
            musicListView.Size = new Size(984, 477);
            musicListView.TabIndex = 3;
            musicListView.UseCompatibleStateImageBehavior = false;
            musicListView.View = View.Details;
            musicListView.Visible = false;
            musicListView.DoubleClick += musicListView_DoubleClick;
            // 
            // id
            // 
            id.Text = "#";
            id.Width = 30;
            // 
            // fileName
            // 
            fileName.Text = "Nazev souboru";
            fileName.Width = 500;
            // 
            // duration
            // 
            duration.Text = "Délka";
            duration.Width = 70;
            // 
            // current
            // 
            current.Text = "";
            current.Width = 100;
            // 
            // playerForm
            // 
            AutoScaleMode = AutoScaleMode.None;
            BackColor = Color.White;
            ClientSize = new Size(984, 581);
            Controls.Add(openFilesButton);
            Controls.Add(musicListView);
            Controls.Add(controlsPanel);
            Controls.Add(menuStrip1);
            Icon = (Icon)resources.GetObject("$this.Icon");
            MainMenuStrip = menuStrip1;
            Name = "playerForm";
            Text = "Music player";
            menuStrip1.ResumeLayout(false);
            menuStrip1.PerformLayout();
            controlsPanel.ResumeLayout(false);
            controlsPanel.PerformLayout();
            progressBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)volumeTrack).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private MenuStrip menuStrip1;
        private ToolStripMenuItem mediaToolStripMenuItem;
        private ToolStripMenuItem openDirectoryStripMenuItem;
        private ToolStripMenuItem openFileStripMenuItem;
        private ToolStripMenuItem exitStripMenuItem;
        private Panel controlsPanel;
        private ToolStripMenuItem pomocToolStripMenuItem;
        private TrackBar volumeTrack;
        private Label currentTimeLabel;
        private Label durationTimeLabel;
        private ToolStripMenuItem openFilesStripMenuItem;
        private FontAwesome.Sharp.IconButton togglePlayButton;
        private FontAwesome.Sharp.IconButton forwardButton;
        private FontAwesome.Sharp.IconButton backwardButton;
        private System.Windows.Forms.Timer timer1;
        private Label songNameLabel;
        private Panel progressBar;
        private Panel progressIndicator;
        private FontAwesome.Sharp.IconButton openFilesButton;
        private ToolStripMenuItem clearQueueStripMenuItem;
        private ToolStripMenuItem helpStripMenuItem;
        private ToolStripMenuItem aboutAppStripMenuItem;
        private FontAwesome.Sharp.IconButton toggleMuteButton;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripMenuItem removeFromPlaylistStripMenuItem;
        private ListView musicListView;
        private ColumnHeader id;
        private ColumnHeader fileName;
        private ColumnHeader current;
        private ColumnHeader duration;
    }
}