using System.Windows.Media;
using System.Drawing;
using System.Media;
using System.Diagnostics;
using System.Numerics;
using NAudio.Wave;

namespace music_player
{
    public partial class playerForm : Form
    {
        private WMPLib.WindowsMediaPlayer player;
        private int activeSong;
        private bool isPaused;
        public playerForm()
        {
            InitializeComponent();
            positionButtons(null, null);

            activeSong = -1;
            isPaused = true;
            //Set minimum window size
            this.MinimumSize = new Size(this.Size.Width, this.Size.Height);

            //Player
            player = new WMPLib.WindowsMediaPlayer();
            player.PlayStateChange += new WMPLib._WMPOCXEvents_PlayStateChangeEventHandler(playerStateChange);

            //Menu strip renderer
            menuStrip1.Renderer = new MyRenderer();

            //Set value
            volumeTrack.Value = player.settings.volume;

            //Resize
            this.SizeChanged += positionButtons;
        }

        private void positionButtons(object? sender, EventArgs? e)
        {
            //Center button
            openFilesButton.Left = (this.Size.Width / 2) - (openFilesButton.Width / 2);
            openFilesButton.Top = (this.musicListView.Height / 2) - (openFilesButton.Height / 2) + (menuStrip1.Height);

            //Center controls
            togglePlayButton.Left = (this.Size.Width / 2) - (togglePlayButton.Width / 2);
            backwardButton.Left = (this.Size.Width / 2) - (backwardButton.Width / 2) - 50;
            forwardButton.Left = (this.Size.Width / 2) - (backwardButton.Width / 2) + 50;
        }

        private void playerStateChange(int newState)
        {
            if ((WMPLib.WMPPlayState)newState == WMPLib.WMPPlayState.wmppsMediaEnded)
            {
                playForward();
            }

            if ((WMPLib.WMPPlayState)newState == WMPLib.WMPPlayState.wmppsReady)
            {
                togglePlay();
            }

            if ((WMPLib.WMPPlayState)newState == WMPLib.WMPPlayState.wmppsPaused)
            {
                togglePlayButton.IconChar = FontAwesome.Sharp.IconChar.Play;
            }

            if ((WMPLib.WMPPlayState)newState == WMPLib.WMPPlayState.wmppsStopped)
            {
                togglePlayButton.IconChar = FontAwesome.Sharp.IconChar.Play;
            }

            if ((WMPLib.WMPPlayState)newState == WMPLib.WMPPlayState.wmppsPlaying)
            {
                togglePlayButton.IconChar = FontAwesome.Sharp.IconChar.Pause;
            }
        }

        private void musicListView_DoubleClick(object sender, EventArgs e)
        {
            if (musicListView.SelectedItems.Count == 0)
            {
                return;
            }

            ListViewItem item = musicListView.SelectedItems[0];
            int index = int.Parse(item.SubItems[0].Text);
            changeAudioSource(index);
        }

        private void forwardButton_Click(object sender, EventArgs e)
        {
            playForward();
            songNameLabel.Focus();
        }

        private void backwardButton_Click(object sender, EventArgs e)
        {
            playBackward();
            songNameLabel.Focus();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (player.playState == WMPLib.WMPPlayState.wmppsPlaying)
            {
                double percentage = (double)player.controls.currentPosition / (double)player.controls.currentItem.duration;
                double width = progressBar.Width * percentage;
                progressIndicator.Width = Convert.ToInt32(width);

                currentTimeLabel.Text = player.controls.currentPositionString;
                durationTimeLabel.Text = player.controls.currentItem.durationString;
            }
        }

        private void progressBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (player.URL != "")
            {
                player.controls.currentPosition = player.currentMedia.duration * ((double)e.X / (double)progressBar.Width);
                progressIndicator.Width = e.X;
            }
        }

        private void togglePlayButton_Click(object sender, EventArgs e)
        {
            togglePlay();
        }

        private void openDirectoryStripMenuItem_Click(object sender, EventArgs e)
        {
            string[] files = getFilesFromDirectory();
            loadFiles(files);
        }

        private void odebratSouboryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            clearLoadedFiles();
        }

        private void removeFromPlaylistStripMenuItem_Click(object sender, EventArgs e)
        {
            removeFromPlaylist();
        }

        private void openFilesButton_Click(object sender, EventArgs e)
        {
            string[] files = getFiles();
            loadFiles(files);
        }

        private void openFileStripMenuItem_Click(object sender, EventArgs e)
        {
            string[] file = { getFile() };
            loadFiles(file);
        }

        private void openFilesStripMenuItem_Click(object sender, EventArgs e)
        {
            string[] files = getFiles();
            loadFiles(files);

        }

        private void exitStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void helpStripMenuItem_Click(object sender, EventArgs e)
        {
            helpForm form = new helpForm();
            form.Show();
        }

        private void aboutAppStripMenuItem_Click(object sender, EventArgs e)
        {
            aboutForm form = new aboutForm();
            form.Show();
        }

        private void volumeTrack_Scroll(object sender, EventArgs e)
        {
            changeVolume(volumeTrack.Value);
        }

        private void toggleMuteButton_Click(object sender, EventArgs e)
        {
            toggleMute();
            songNameLabel.Focus();
        }

        private void togglePlay()
        {
            if (isPaused)
            {
                if (player.URL == "")
                {
                    int playIndex;
                    if (musicListView.SelectedItems.Count == 0)
                    {
                        if (activeSong < 0)
                        {
                            playIndex = 1;
                        }

                        else
                        {
                            playIndex = activeSong;
                        }
                    }

                    else
                    {
                        ListViewItem item = musicListView.SelectedItems[0];
                        playIndex = int.Parse(item.SubItems[0].Text);
                    }

                    changeAudioSource(playIndex);
                }

                try
                {
                    player.controls.play();
                }

                catch (Exception) { }

                isPaused = false;
            }

            else
            {
                try
                {
                    player.controls.pause();
                }

                catch (Exception) { }
                isPaused = true;
            }
        }

        private void changeVolume(int volume)
        {
            player.settings.volume = volume;
            player.settings.mute = false;
            toggleMuteButton.IconChar = FontAwesome.Sharp.IconChar.VolumeHigh;
        }

        private void toggleMute()
        {
            if (player.settings.mute)
            {
                player.settings.mute = false;
                toggleMuteButton.IconChar = FontAwesome.Sharp.IconChar.VolumeHigh;
                volumeTrack.Value = player.settings.volume;
            }

            else
            {
                player.settings.mute = true;
                toggleMuteButton.IconChar = FontAwesome.Sharp.IconChar.VolumeMute;
                volumeTrack.Value = 0;
            }
        }

        private void changeAudioSource(int sourceId)
        {
            if (sourceId <= 0)
            {
                return;
            }

            if (sourceId > musicListView.Items.Count)
            {
                return;
            }

            ListViewItem item = musicListView.Items[sourceId - 1];

            foreach (ListViewItem currentItem in musicListView.Items)
            {
                currentItem.SubItems[3].Text = "";
            }

            item.SubItems[3].Text = "P�ehr�v� se...";

            player.URL = item.SubItems[1].Text;
            //Print song name to controls
            songNameLabel.Text = player.controls.currentItem.name;
            activeSong = sourceId;
        }

        private void playForward()
        {
            if (musicListView.Items.Count == 0)
            {
                return;
            }

            int newIndex = activeSong;

            if (newIndex < musicListView.Items.Count)
            {
                newIndex += 1;
            }

            else
            {
                newIndex = 1;
            }

            changeAudioSource(newIndex);
        }

        private void playBackward()
        {
            if (musicListView.Items.Count == 0)
            {
                return;
            }

            int newIndex = activeSong;

            if (newIndex > 1)
            {
                newIndex -= 1;
            }

            else
            {
                newIndex = musicListView.Items.Count;
            }

            changeAudioSource(newIndex);
        }

        private void loadFiles(string[] files)
        {
            if (files.Length == 0)
            {
                return;
            }

            if (musicListView.Items.Count > 0)
            {
                string message = "Chcete skladby p�idat do playlistu nebo playlist vypr�zdnit? \n Ano - P�idat \n Ne - Vy�istit";
                string title = "P�id�n� skladeb";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result = MessageBox.Show(message, title, buttons);
                if (result == DialogResult.No)
                {
                    musicListView.Items.Clear();
                }
            }

            openFilesButton.Hide();
            musicListView.Show();

            removeFromPlaylistStripMenuItem.Enabled = true;
            clearQueueStripMenuItem.Enabled = true;

            for (int i = 0; i < files.Length; i++)
            {
                string durationString;
                if (files[i].EndsWith(".wav"))
                {
                    WaveFileReader wf = new WaveFileReader(files[i]);
                    TimeSpan duration = wf.TotalTime;
                    durationString = $"{(duration.Hours >= 10 ? duration.Hours : "0" + duration.Hours)}:{(duration.Minutes >= 10 ? duration.Minutes : "0" + duration.Minutes)}:{(duration.Seconds >= 10 ? duration.Seconds : "0" + duration.Seconds)}";
                }

                else if (files[i].EndsWith(".mp3"))
                {
                    Mp3FileReader pm = new Mp3FileReader(files[i]);
                    TimeSpan duration = pm.TotalTime;
                    durationString = $"{(duration.Hours >= 10 ? duration.Hours : "0" + duration.Hours)}:{(duration.Minutes >= 10 ? duration.Minutes : "0" + duration.Minutes)}:{(duration.Seconds >= 10 ? duration.Seconds : "0" + duration.Seconds)}";
                }

                else
                {
                    return;
                }


                ListViewItem item = new ListViewItem((musicListView.Items.Count + 1).ToString());
                item.SubItems.Add(files[i]);
                item.SubItems.Add(durationString);
                item.SubItems.Add("");
                musicListView.Items.Add(item);
            }
        }

        private void removeFromPlaylist()
        {
            if (musicListView.SelectedItems.Count == 0)
            {
                MessageBox.Show("Vyberte skladbu kterou chcete z playlistu odebrat");
                return;
            }

            foreach (ListViewItem item in musicListView.SelectedItems)
            {

                musicListView.Items.Remove(item);

                if (player.URL == item.SubItems[1].Text)
                {
                    clearCurrentPlaying();
                }
            }

            if (musicListView.Items.Count == 0)
            {
                removeFromPlaylistStripMenuItem.Enabled = false;
                clearQueueStripMenuItem.Enabled = false;
                clearLoadedFiles();
            }

            //Fix indexes
            for (int i = 0; i < musicListView.Items.Count; i++)
            {
                musicListView.Items[i].SubItems[0].Text = (i + 1).ToString();
            }
        }

        private void clearLoadedFiles()
        {
            clearCurrentPlaying();
            removeFromPlaylistStripMenuItem.Enabled = false;
            clearQueueStripMenuItem.Enabled = false;
            musicListView.Items.Clear();
            musicListView.Hide();
            openFilesButton.Show();
        }

        private void clearCurrentPlaying()
        {
            songNameLabel.Text = "Skladba nevybr�na";
            currentTimeLabel.Text = "00:00";
            durationTimeLabel.Text = "00:00";
            progressIndicator.Width = 0;
            player.controls.stop();
            player.URL = "";
            activeSong = -1;
        }

        private string getFile()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.RestoreDirectory = true;
            dialog.Filter = "Audio soubory| *.mp3;*.wav";
            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                return dialog.FileName;
            }

            return "";
        }

        private string[] getFiles()
        {
            string[] audioFiles = { };
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.RestoreDirectory = true;
            dialog.Multiselect = true;
            dialog.Filter = "Audio soubory | *.mp3;*.wav";
            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                return dialog.FileNames;
            }

            return audioFiles;
        }

        private String[] getFilesFromDirectory()
        {
            string[] audioFiles = { };
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                audioFiles = Directory.GetFiles(dialog.SelectedPath, "*.*", SearchOption.AllDirectories).Where(file => new string[] { ".mp3", ".wav" }.Contains(Path.GetExtension(file))).ToArray();

                if (audioFiles.Length > 0)
                {
                    return audioFiles;
                }

                else
                {
                    MessageBox.Show("Ve vybran�m adres��i se nenach�z� ��dn� podporovan� audio soubor");
                }
            }

            return audioFiles;
        }

        private class MyRenderer : ToolStripProfessionalRenderer
        {
            public MyRenderer() : base(new MyColors()) { }
        }

        private class MyColors : ProfessionalColorTable
        {
            public override System.Drawing.Color MenuItemSelected
            {
                get { return System.Drawing.Color.FromArgb(33, 37, 41); }
            }

            public override System.Drawing.Color MenuItemPressedGradientBegin
            {
                get { return System.Drawing.Color.FromArgb(33, 37, 41); }
            }

            public override System.Drawing.Color MenuItemPressedGradientEnd
            {
                get { return System.Drawing.Color.FromArgb(33, 37, 41); }
            }

            //Borders

            public override System.Drawing.Color MenuItemBorder
            {
                get { return System.Drawing.Color.Transparent; }
            }

            public override System.Drawing.Color ToolStripBorder
            {
                get { return ColorTranslator.FromHtml("#ff0000"); }
            }
        }
    }
}