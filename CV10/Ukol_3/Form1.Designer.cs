﻿namespace Ukol_3
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            insertGroupBox = new GroupBox();
            insertImageButton = new Button();
            insertImage3 = new RadioButton();
            insertImage2 = new RadioButton();
            insertImage1 = new RadioButton();
            deleteGroupBox = new GroupBox();
            removeImageButton = new Button();
            deleteImage3 = new RadioButton();
            deleteImage2 = new RadioButton();
            deleteImage1 = new RadioButton();
            pictureBox1 = new PictureBox();
            pictureBox2 = new PictureBox();
            pictureBox3 = new PictureBox();
            insertGroupBox.SuspendLayout();
            deleteGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox3).BeginInit();
            SuspendLayout();
            // 
            // insertGroupBox
            // 
            insertGroupBox.Controls.Add(insertImageButton);
            insertGroupBox.Controls.Add(insertImage3);
            insertGroupBox.Controls.Add(insertImage2);
            insertGroupBox.Controls.Add(insertImage1);
            insertGroupBox.Location = new Point(12, 12);
            insertGroupBox.Name = "insertGroupBox";
            insertGroupBox.Size = new Size(200, 100);
            insertGroupBox.TabIndex = 0;
            insertGroupBox.TabStop = false;
            insertGroupBox.Text = "Vložení obrázku";
            // 
            // insertImageButton
            // 
            insertImageButton.Location = new Point(106, 22);
            insertImageButton.Name = "insertImageButton";
            insertImageButton.Size = new Size(88, 69);
            insertImageButton.TabIndex = 3;
            insertImageButton.Text = "Vložit obrázek";
            insertImageButton.UseVisualStyleBackColor = true;
            insertImageButton.Click += insertImageButton_Click;
            // 
            // insertImage3
            // 
            insertImage3.AutoSize = true;
            insertImage3.Location = new Point(6, 72);
            insertImage3.Name = "insertImage3";
            insertImage3.Size = new Size(78, 19);
            insertImage3.TabIndex = 2;
            insertImage3.TabStop = true;
            insertImage3.Text = "3. obrázek";
            insertImage3.UseVisualStyleBackColor = true;
            // 
            // insertImage2
            // 
            insertImage2.AutoSize = true;
            insertImage2.Location = new Point(6, 47);
            insertImage2.Name = "insertImage2";
            insertImage2.Size = new Size(78, 19);
            insertImage2.TabIndex = 1;
            insertImage2.TabStop = true;
            insertImage2.Text = "2. obrázek";
            insertImage2.UseVisualStyleBackColor = true;
            // 
            // insertImage1
            // 
            insertImage1.AutoSize = true;
            insertImage1.Location = new Point(6, 22);
            insertImage1.Name = "insertImage1";
            insertImage1.Size = new Size(78, 19);
            insertImage1.TabIndex = 0;
            insertImage1.TabStop = true;
            insertImage1.Text = "1. obrázek";
            insertImage1.UseVisualStyleBackColor = true;
            // 
            // deleteGroupBox
            // 
            deleteGroupBox.Controls.Add(removeImageButton);
            deleteGroupBox.Controls.Add(deleteImage3);
            deleteGroupBox.Controls.Add(deleteImage2);
            deleteGroupBox.Controls.Add(deleteImage1);
            deleteGroupBox.Location = new Point(235, 12);
            deleteGroupBox.Name = "deleteGroupBox";
            deleteGroupBox.Size = new Size(200, 100);
            deleteGroupBox.TabIndex = 1;
            deleteGroupBox.TabStop = false;
            deleteGroupBox.Text = "Smazání obrázku";
            // 
            // removeImageButton
            // 
            removeImageButton.Location = new Point(106, 22);
            removeImageButton.Name = "removeImageButton";
            removeImageButton.Size = new Size(88, 69);
            removeImageButton.TabIndex = 3;
            removeImageButton.Text = "Smazat obrázek";
            removeImageButton.UseVisualStyleBackColor = true;
            removeImageButton.Click += removeImageButton_Click;
            // 
            // deleteImage3
            // 
            deleteImage3.AutoSize = true;
            deleteImage3.Location = new Point(6, 72);
            deleteImage3.Name = "deleteImage3";
            deleteImage3.Size = new Size(78, 19);
            deleteImage3.TabIndex = 2;
            deleteImage3.TabStop = true;
            deleteImage3.Text = "3. obrázek";
            deleteImage3.UseVisualStyleBackColor = true;
            // 
            // deleteImage2
            // 
            deleteImage2.AutoSize = true;
            deleteImage2.Location = new Point(6, 47);
            deleteImage2.Name = "deleteImage2";
            deleteImage2.Size = new Size(78, 19);
            deleteImage2.TabIndex = 1;
            deleteImage2.TabStop = true;
            deleteImage2.Text = "2. obrázek";
            deleteImage2.UseVisualStyleBackColor = true;
            // 
            // deleteImage1
            // 
            deleteImage1.AutoSize = true;
            deleteImage1.Location = new Point(6, 22);
            deleteImage1.Name = "deleteImage1";
            deleteImage1.Size = new Size(78, 19);
            deleteImage1.TabIndex = 0;
            deleteImage1.TabStop = true;
            deleteImage1.Text = "1. obrázek";
            deleteImage1.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            pictureBox1.BorderStyle = BorderStyle.FixedSingle;
            pictureBox1.Location = new Point(13, 125);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new Size(128, 81);
            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox1.TabIndex = 2;
            pictureBox1.TabStop = false;
            pictureBox1.Click += pictureBox_Click;
            pictureBox1.DoubleClick += pictureBox_DoubleClick;
            // 
            // pictureBox2
            // 
            pictureBox2.BorderStyle = BorderStyle.FixedSingle;
            pictureBox2.Location = new Point(166, 125);
            pictureBox2.Name = "pictureBox2";
            pictureBox2.Size = new Size(128, 81);
            pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox2.TabIndex = 5;
            pictureBox2.TabStop = false;
            pictureBox2.Click += pictureBox_Click;
            pictureBox2.DoubleClick += pictureBox_DoubleClick;
            // 
            // pictureBox3
            // 
            pictureBox3.BorderStyle = BorderStyle.FixedSingle;
            pictureBox3.Location = new Point(307, 125);
            pictureBox3.Name = "pictureBox3";
            pictureBox3.Size = new Size(128, 81);
            pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox3.TabIndex = 6;
            pictureBox3.TabStop = false;
            pictureBox3.Click += pictureBox_Click;
            pictureBox3.DoubleClick += pictureBox_DoubleClick;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(445, 218);
            Controls.Add(pictureBox3);
            Controls.Add(pictureBox2);
            Controls.Add(pictureBox1);
            Controls.Add(deleteGroupBox);
            Controls.Add(insertGroupBox);
            Name = "Form1";
            Text = "Galerie";
            insertGroupBox.ResumeLayout(false);
            insertGroupBox.PerformLayout();
            deleteGroupBox.ResumeLayout(false);
            deleteGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox2).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox3).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private GroupBox insertGroupBox;
        private Button insertImageButton;
        private RadioButton insertImage3;
        private RadioButton insertImage2;
        private RadioButton insertImage1;
        private GroupBox deleteGroupBox;
        private Button removeImageButton;
        private RadioButton deleteImage3;
        private RadioButton deleteImage2;
        private RadioButton deleteImage1;
        private PictureBox pictureBox1;
        private PictureBox pictureBox2;
        private PictureBox pictureBox3;
    }
}