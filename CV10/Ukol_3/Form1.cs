using System.Security.Cryptography;
using System.Windows.Forms;

namespace Ukol_3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void insertImageButton_Click(object sender, EventArgs e)
        {
            switch (getSelectedRadioInsert())
            {
                case "insertImage1":
                    if (pictureBox1.Image != null)
                    {
                        MessageBox.Show("Vybran� box ji� obr�zek obsahuje");
                        return;
                    }
                    break;

                case "insertImage2":
                    if (pictureBox2.Image != null)
                    {
                        MessageBox.Show("Vybran� box ji� obr�zek obsahuje");
                        return;
                    }
                    break;

                case "insertImage3":
                    if (pictureBox2.Image != null)
                    {
                        MessageBox.Show("Vybran� box ji� obr�zek obsahuje");
                        return;
                    }
                    break;
            }

            //c# open file dialog with image filters
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp; *.png)|*.jpg; *.jpeg; *.gif; *.bmp; *.png";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                //c# display image in picture box�
                //pictureBox1.Image = new Bitmap(ofd.FileName);

                switch (getSelectedRadioInsert())
                {
                    case "insertImage1":
                        pictureBox1.Image = new Bitmap(ofd.FileName);
                        break;

                    case "insertImage2":
                        pictureBox2.Image = new Bitmap(ofd.FileName);
                        break;

                    case "insertImage3":
                        pictureBox3.Image = new Bitmap(ofd.FileName);
                        break;
                }
                //c# image file path�
                //txtFileName.Text = ofd.FileName;
            }
        }
        private void removeImageButton_Click(object sender, EventArgs e)
        {
            switch (getSelectedRadioDelete())
            {
                case "deleteImage1":
                    pictureBox1.Image = null;
                    break;

                case "deleteImage2":
                    pictureBox2.Image = null;
                    break;

                case "deleteImage3":
                    pictureBox3.Image = null;
                    break;
            }
        }
        private void pictureBox_Click(object sender, EventArgs e)
        {
            switch (((PictureBox)sender).Name)
            {
                case "pictureBox1":
                    insertImage1.Checked = true;
                    break;

                case "pictureBox2":
                    insertImage2.Checked = true;
                    break;

                case "pictureBox3":
                    insertImage3.Checked = true;
                    break;
            }
        }
        private void pictureBox_DoubleClick(object sender, EventArgs e)
        {

            Form form = new Form();

            form.Width = 300;
            form.Height = 300;
            form.BackgroundImageLayout = ImageLayout.Zoom;

            switch (((PictureBox)sender).Name)
            {
                case "pictureBox1":
                    form.BackgroundImage = pictureBox1.Image;
                    break;

                case "pictureBox2":
                    form.BackgroundImage = pictureBox2.Image;
                    break;

                case "pictureBox3":
                    form.BackgroundImage = pictureBox3.Image;
                    break;
            }

            form.Visible = true;
        }

        private string getSelectedRadioInsert()
        {
            IEnumerable<RadioButton> radioButtons = insertGroupBox.Controls.OfType<RadioButton>();

            if (radioButtons.Count() == 0)
            {
                return "";
            }

            RadioButton? checkedRadio = radioButtons.FirstOrDefault(rb => rb.Checked);

            if (checkedRadio == null)
            {
                return "";
            }

            return checkedRadio.Name;
        }

        private string getSelectedRadioDelete()
        {
            IEnumerable<RadioButton> radioButtons = deleteGroupBox.Controls.OfType<RadioButton>();

            if (radioButtons.Count() == 0)
            {
                return "";
            }

            RadioButton? checkedRadio = radioButtons.FirstOrDefault(rb => rb.Checked);

            if (checkedRadio == null)
            {
                return "";
            }

            return checkedRadio.Name;
        }
    }
}