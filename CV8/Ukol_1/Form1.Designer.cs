﻿namespace Ukol_1
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            numberBox1 = new TextBox();
            numberBox2 = new TextBox();
            operationBox = new ComboBox();
            outputLabel = new Label();
            calculateButton = new Button();
            SuspendLayout();
            // 
            // numberBox1
            // 
            resources.ApplyResources(numberBox1, "numberBox1");
            numberBox1.Name = "numberBox1";
            // 
            // numberBox2
            // 
            resources.ApplyResources(numberBox2, "numberBox2");
            numberBox2.Name = "numberBox2";
            // 
            // operationBox
            // 
            resources.ApplyResources(operationBox, "operationBox");
            operationBox.DisplayMember = "+";
            operationBox.DropDownStyle = ComboBoxStyle.DropDownList;
            operationBox.FormattingEnabled = true;
            operationBox.Items.AddRange(new object[] { resources.GetString("operationBox.Items"), resources.GetString("operationBox.Items1"), resources.GetString("operationBox.Items2"), resources.GetString("operationBox.Items3"), resources.GetString("operationBox.Items4"), resources.GetString("operationBox.Items5") });
            operationBox.Name = "operationBox";
            // 
            // outputLabel
            // 
            resources.ApplyResources(outputLabel, "outputLabel");
            outputLabel.ForeColor = Color.ForestGreen;
            outputLabel.Name = "outputLabel";
            // 
            // calculateButton
            // 
            resources.ApplyResources(calculateButton, "calculateButton");
            calculateButton.Name = "calculateButton";
            calculateButton.UseVisualStyleBackColor = true;
            calculateButton.Click += calculateButton_Click;
            // 
            // Form1
            // 
            resources.ApplyResources(this, "$this");
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(calculateButton);
            Controls.Add(outputLabel);
            Controls.Add(operationBox);
            Controls.Add(numberBox2);
            Controls.Add(numberBox1);
            Name = "Form1";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private TextBox numberBox1;
        private TextBox numberBox2;
        private ComboBox operationBox;
        private Label outputLabel;
        private Button calculateButton;
    }
}