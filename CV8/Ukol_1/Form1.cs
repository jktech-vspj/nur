namespace Ukol_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            string selectedOperator = operationBox.Text;

            string input1 = numberBox1.Text;
            string input2 = numberBox2.Text;

            double number1, number2;

            if (string.IsNullOrEmpty(input1))
            {
                showMessage("��slo 1 nem��e b�t pr�zdn�");
                return;
            }

            if (!double.TryParse(input1, out number1))
            {
                showMessage("��slo 1 nen� ��slo");
                return;
            }

            if (string.IsNullOrEmpty(selectedOperator))
            {
                showMessage("Oper�tor nem��e b�t pr�zdn�");
                return;
            }

            if (string.IsNullOrEmpty(input2))
            {
                showMessage("��slo 2 nem��e b�t pr�zdn�");
                return;
            }

            if (!double.TryParse(input2, out number2))
            {
                showMessage("��slo 2 nen� ��slo");
                return;
            }

            string result;

            switch (selectedOperator)
            {
                case "+":
                    result = (number1 + number2).ToString();
                    break;

                case "-":
                    result = (number1 - number2).ToString();
                    break;

                case "*":
                    result = (number1 * number2).ToString();
                    break;

                case "/":
                    if (number2 == 0)
                    {
                        showMessage("Nelze delit nulou");
                        return;
                    }
                    result = (number1 / number2).ToString();
                    break;

                case "mocnina":
                    result = Math.Pow(number1, number2).ToString();
                    break;

                case "odmocnina":
                    bool success = false;
                    if (number1 > 0)
                    {
                        success = true;
                        result = Math.Sqrt(number1).ToString();
                    }

                    else
                    {
                        result = "";
                    }

                    if (number2 > 0)
                    {
                        if (success == true)
                        {
                            result += ", " + Math.Sqrt(number2).ToString();
                        }

                        else
                        {
                            success = true;
                            result += Math.Sqrt(number2).ToString();
                        }
                    }

                    if (success == false)
                    {
                        showMessage("Nelze odmocnit ani jedno ��slo");
                        return;
                    }

                    break;

                default:
                    showMessage("Neplatn� oper�tor");
                    return;
            }

            outputLabel.Text = result;
            clearInputs();
        }

        private void clearInputs()
        {
            numberBox1.Text = "";
            numberBox2.Text = "";

            operationBox.Text = "Vyber operaci";
        }

        private void showMessage(string message)
        {
            MessageBoxButtons buttons = MessageBoxButtons.OK;
            DialogResult result;

            result = MessageBox.Show(message, "Kalkula�ka", buttons);
        }
    }
}