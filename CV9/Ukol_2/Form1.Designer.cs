﻿namespace Ukol_2
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ColorsComboBox = new ComboBox();
            selectedColorslistBox = new ListBox();
            colorsListBox = new CheckedListBox();
            addColorButton = new Button();
            deleteSelectedColorButton = new Button();
            closeProgramButton = new Button();
            selectSelectedColorsButton = new Button();
            SuspendLayout();
            // 
            // ColorsComboBox
            // 
            ColorsComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            ColorsComboBox.FormattingEnabled = true;
            ColorsComboBox.Location = new Point(12, 219);
            ColorsComboBox.Name = "ColorsComboBox";
            ColorsComboBox.Size = new Size(121, 23);
            ColorsComboBox.TabIndex = 0;
            // 
            // selectedColorslistBox
            // 
            selectedColorslistBox.FormattingEnabled = true;
            selectedColorslistBox.ItemHeight = 15;
            selectedColorslistBox.Location = new Point(12, 12);
            selectedColorslistBox.Name = "selectedColorslistBox";
            selectedColorslistBox.Size = new Size(121, 199);
            selectedColorslistBox.TabIndex = 1;
            selectedColorslistBox.MouseDoubleClick += selectedColorslistBox_MouseDoubleClick;
            // 
            // colorsListBox
            // 
            colorsListBox.ForeColor = Color.Black;
            colorsListBox.FormattingEnabled = true;
            colorsListBox.Location = new Point(207, 12);
            colorsListBox.Name = "colorsListBox";
            colorsListBox.SelectionMode = SelectionMode.None;
            colorsListBox.Size = new Size(114, 202);
            colorsListBox.TabIndex = 2;
            // 
            // addColorButton
            // 
            addColorButton.Location = new Point(12, 248);
            addColorButton.Name = "addColorButton";
            addColorButton.Size = new Size(121, 23);
            addColorButton.TabIndex = 3;
            addColorButton.Text = "Přidej barvu";
            addColorButton.UseVisualStyleBackColor = true;
            addColorButton.Click += addColorButton_Click;
            // 
            // deleteSelectedColorButton
            // 
            deleteSelectedColorButton.Location = new Point(12, 277);
            deleteSelectedColorButton.Name = "deleteSelectedColorButton";
            deleteSelectedColorButton.Size = new Size(121, 42);
            deleteSelectedColorButton.TabIndex = 4;
            deleteSelectedColorButton.Text = "Smaž vybranou barvu";
            deleteSelectedColorButton.UseVisualStyleBackColor = true;
            deleteSelectedColorButton.Click += deleteSelectedColorButton_Click;
            // 
            // closeProgramButton
            // 
            closeProgramButton.Location = new Point(207, 220);
            closeProgramButton.Name = "closeProgramButton";
            closeProgramButton.Size = new Size(114, 99);
            closeProgramButton.TabIndex = 5;
            closeProgramButton.Text = "ZAVŘÍT PROGRAM";
            closeProgramButton.UseVisualStyleBackColor = true;
            closeProgramButton.Click += closeProgramButton_Click;
            // 
            // selectSelectedColorsButton
            // 
            selectSelectedColorsButton.Location = new Point(139, 12);
            selectSelectedColorsButton.Name = "selectSelectedColorsButton";
            selectSelectedColorsButton.Size = new Size(62, 202);
            selectSelectedColorsButton.TabIndex = 6;
            selectSelectedColorsButton.Text = "Vyber dané barvy";
            selectSelectedColorsButton.UseVisualStyleBackColor = true;
            selectSelectedColorsButton.Click += selectSelectedColorsButton_Click;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(332, 329);
            Controls.Add(selectSelectedColorsButton);
            Controls.Add(closeProgramButton);
            Controls.Add(deleteSelectedColorButton);
            Controls.Add(addColorButton);
            Controls.Add(colorsListBox);
            Controls.Add(selectedColorslistBox);
            Controls.Add(ColorsComboBox);
            Name = "Form1";
            Text = "Barvy";
            ResumeLayout(false);
        }

        #endregion

        private ComboBox ColorsComboBox;
        private ListBox selectedColorslistBox;
        private CheckedListBox colorsListBox;
        private Button addColorButton;
        private Button deleteSelectedColorButton;
        private Button closeProgramButton;
        private Button selectSelectedColorsButton;
    }
}