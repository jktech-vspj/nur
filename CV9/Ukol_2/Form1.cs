using System.Windows.Forms;

namespace Ukol_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            string[] colors = new string[] { "�ern�", "�erven�", "Modr�", "�lut�", "Oran�ov�", "Zelen�", "Hn�d�", "B�l�" };
            //Add values to controls
            for (int i = 0; i < colors.Length; i++)
            {
                colorsListBox.Items.Add(colors[i]);
                ColorsComboBox.Items.Add(colors[i]);
            }

            ColorsComboBox.SelectedIndex = 0;

        }

        private void addColorButton_Click(object sender, EventArgs e)
        {
            string selectedColor = ColorsComboBox.Text;
            if (selectedColor.Length == 0)
            {
                MessageBox.Show("Nen� vybr�na ��dn� barva");
                return;
            }

            if (selectedColorslistBox.Items.Contains(selectedColor))
            {
                MessageBox.Show("Vybran� barva u� je v seznamu");
                return;
            }

            selectedColorslistBox.Items.Add(selectedColor);
        }

        private void deleteSelectedColorButton_Click(object sender, EventArgs e)
        {
            int selectedValue = selectedColorslistBox.SelectedIndex;
            if (selectedValue < 0)
            {
                return;
            }

            selectedColorslistBox.Items.Remove(selectedColorslistBox.Items[selectedValue]);
            this.updateCheckboxes();
        }
        private void selectSelectedColorsButton_Click(object sender, EventArgs e)
        {
            this.updateCheckboxes();
        }

        private void selectedColorslistBox_MouseDoubleClick(object sender, EventArgs e)
        {
            if (selectedColorslistBox.SelectedIndex < 0)
            {
                MessageBox.Show("Nen� vybr�na ��dn� barva");
                return;
            }

            Form form = new Form();
            switch (selectedColorslistBox.SelectedItem.ToString())
            {
                case "�ern�":
                    form.BackColor = Color.Black;
                    break;

                case "�erven�":
                    form.BackColor = Color.Red;
                    break;

                case "Modr�":
                    form.BackColor = Color.Blue;
                    break;

                case "�lut�":
                    form.BackColor = Color.Yellow;
                    break;

                case "Oran�ov�":
                    form.BackColor = Color.Orange;
                    break;

                case "Zelen�":
                    form.BackColor = Color.Green;
                    break;

                case "Hn�d�":
                    form.BackColor = Color.Brown;
                    break;

                case "B�l�":
                    form.BackColor = Color.White;
                    break;
            }

            form.Text = "Okno " + selectedColorslistBox.SelectedItem.ToString();
            form.Visible = true;

        }

        private void closeProgramButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void updateCheckboxes()
        {
            for (int i = 0; i < colorsListBox.Items.Count; i++)
            {
                colorsListBox.SetItemChecked(i, false);
            }

            for (int i = 0; i < selectedColorslistBox.Items.Count; i++)
            {
                int itemIndex = colorsListBox.Items.IndexOf(selectedColorslistBox.Items[i]);
                colorsListBox.SetItemChecked(itemIndex, true);
            }
        }
    }
}