﻿namespace Ukol_4
{
    partial class Osoby
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            nameListBox = new ListBox();
            lastnameListBox = new ListBox();
            nameTextBox = new TextBox();
            lastnameTextBox = new TextBox();
            insertName = new Button();
            insertLastname = new Button();
            label1 = new Label();
            label2 = new Label();
            menuStrip1 = new MenuStrip();
            souborToolStripMenuItem = new ToolStripMenuItem();
            exportToolStripMenuItem = new ToolStripMenuItem();
            jménoToolStripMenuItem = new ToolStripMenuItem();
            příjmeníToolStripMenuItem = new ToolStripMenuItem();
            jménoPříjmeníToolStripMenuItem = new ToolStripMenuItem();
            importToolStripMenuItem = new ToolStripMenuItem();
            jménoPříjmeníToolStripMenuItem1 = new ToolStripMenuItem();
            konecToolStripMenuItem = new ToolStripMenuItem();
            menuStrip1.SuspendLayout();
            SuspendLayout();
            // 
            // nameListBox
            // 
            nameListBox.FormattingEnabled = true;
            nameListBox.ItemHeight = 15;
            nameListBox.Location = new Point(12, 49);
            nameListBox.Name = "nameListBox";
            nameListBox.Size = new Size(120, 184);
            nameListBox.TabIndex = 0;
            // 
            // lastnameListBox
            // 
            lastnameListBox.FormattingEnabled = true;
            lastnameListBox.ItemHeight = 15;
            lastnameListBox.Location = new Point(138, 49);
            lastnameListBox.Name = "lastnameListBox";
            lastnameListBox.Size = new Size(120, 184);
            lastnameListBox.TabIndex = 1;
            // 
            // nameTextBox
            // 
            nameTextBox.Location = new Point(12, 240);
            nameTextBox.Name = "nameTextBox";
            nameTextBox.Size = new Size(120, 23);
            nameTextBox.TabIndex = 2;
            // 
            // lastnameTextBox
            // 
            lastnameTextBox.Location = new Point(138, 240);
            lastnameTextBox.Name = "lastnameTextBox";
            lastnameTextBox.Size = new Size(120, 23);
            lastnameTextBox.TabIndex = 3;
            // 
            // insertName
            // 
            insertName.Location = new Point(12, 269);
            insertName.Name = "insertName";
            insertName.Size = new Size(120, 23);
            insertName.TabIndex = 4;
            insertName.Text = "Vlož jméno";
            insertName.UseVisualStyleBackColor = true;
            insertName.Click += insertName_Click;
            // 
            // insertLastname
            // 
            insertLastname.Location = new Point(138, 269);
            insertLastname.Name = "insertLastname";
            insertLastname.Size = new Size(120, 23);
            insertLastname.TabIndex = 5;
            insertLastname.Text = "Vlož příjmení";
            insertLastname.UseVisualStyleBackColor = true;
            insertLastname.Click += insertLastname_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(12, 31);
            label1.Name = "label1";
            label1.Size = new Size(45, 15);
            label1.TabIndex = 6;
            label1.Text = "JMÉNA";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(138, 31);
            label2.Name = "label2";
            label2.Size = new Size(57, 15);
            label2.TabIndex = 7;
            label2.Text = "PŘÍJMENÍ";
            // 
            // menuStrip1
            // 
            menuStrip1.Items.AddRange(new ToolStripItem[] { souborToolStripMenuItem });
            menuStrip1.Location = new Point(0, 0);
            menuStrip1.Name = "menuStrip1";
            menuStrip1.Size = new Size(270, 24);
            menuStrip1.TabIndex = 8;
            menuStrip1.Text = "menuStrip1";
            // 
            // souborToolStripMenuItem
            // 
            souborToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { exportToolStripMenuItem, importToolStripMenuItem, konecToolStripMenuItem });
            souborToolStripMenuItem.Name = "souborToolStripMenuItem";
            souborToolStripMenuItem.Size = new Size(57, 20);
            souborToolStripMenuItem.Text = "Soubor";
            // 
            // exportToolStripMenuItem
            // 
            exportToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { jménoToolStripMenuItem, příjmeníToolStripMenuItem, jménoPříjmeníToolStripMenuItem });
            exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            exportToolStripMenuItem.Size = new Size(180, 22);
            exportToolStripMenuItem.Text = "Export";
            // 
            // jménoToolStripMenuItem
            // 
            jménoToolStripMenuItem.Name = "jménoToolStripMenuItem";
            jménoToolStripMenuItem.Size = new Size(167, 22);
            jménoToolStripMenuItem.Text = "Jméno";
            jménoToolStripMenuItem.Click += jménoToolStripMenuItem_Click;
            // 
            // příjmeníToolStripMenuItem
            // 
            příjmeníToolStripMenuItem.Name = "příjmeníToolStripMenuItem";
            příjmeníToolStripMenuItem.Size = new Size(167, 22);
            příjmeníToolStripMenuItem.Text = "Příjmení";
            příjmeníToolStripMenuItem.Click += příjmeníToolStripMenuItem_Click;
            // 
            // jménoPříjmeníToolStripMenuItem
            // 
            jménoPříjmeníToolStripMenuItem.Name = "jménoPříjmeníToolStripMenuItem";
            jménoPříjmeníToolStripMenuItem.Size = new Size(167, 22);
            jménoPříjmeníToolStripMenuItem.Text = "Jméno + Příjmení";
            jménoPříjmeníToolStripMenuItem.Click += jménoPříjmeníToolStripMenuItem_Click;
            // 
            // importToolStripMenuItem
            // 
            importToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { jménoPříjmeníToolStripMenuItem1 });
            importToolStripMenuItem.Name = "importToolStripMenuItem";
            importToolStripMenuItem.Size = new Size(180, 22);
            importToolStripMenuItem.Text = "Import";
            // 
            // jménoPříjmeníToolStripMenuItem1
            // 
            jménoPříjmeníToolStripMenuItem1.Name = "jménoPříjmeníToolStripMenuItem1";
            jménoPříjmeníToolStripMenuItem1.Size = new Size(180, 22);
            jménoPříjmeníToolStripMenuItem1.Text = "Jméno + Příjmení";
            jménoPříjmeníToolStripMenuItem1.Click += jménoPříjmeníToolStripMenuItem1_Click;
            // 
            // konecToolStripMenuItem
            // 
            konecToolStripMenuItem.Name = "konecToolStripMenuItem";
            konecToolStripMenuItem.Size = new Size(180, 22);
            konecToolStripMenuItem.Text = "Konec";
            konecToolStripMenuItem.Click += konecToolStripMenuItem_Click;
            // 
            // Osoby
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(270, 301);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(insertLastname);
            Controls.Add(insertName);
            Controls.Add(lastnameTextBox);
            Controls.Add(nameTextBox);
            Controls.Add(lastnameListBox);
            Controls.Add(nameListBox);
            Controls.Add(menuStrip1);
            MainMenuStrip = menuStrip1;
            Name = "Osoby";
            Text = "Osoby";
            menuStrip1.ResumeLayout(false);
            menuStrip1.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private ListBox nameListBox;
        private ListBox lastnameListBox;
        private TextBox nameTextBox;
        private TextBox lastnameTextBox;
        private Button insertName;
        private Button insertLastname;
        private Label label1;
        private Label label2;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem souborToolStripMenuItem;
        private ToolStripMenuItem exportToolStripMenuItem;
        private ToolStripMenuItem jménoToolStripMenuItem;
        private ToolStripMenuItem příjmeníToolStripMenuItem;
        private ToolStripMenuItem jménoPříjmeníToolStripMenuItem;
        private ToolStripMenuItem importToolStripMenuItem;
        private ToolStripMenuItem jménoPříjmeníToolStripMenuItem1;
        private ToolStripMenuItem konecToolStripMenuItem;
    }
}