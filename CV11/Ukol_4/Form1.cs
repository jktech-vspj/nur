using System.ComponentModel;
using System.Windows.Forms;

namespace Ukol_4
{
    public partial class Osoby : Form
    {
        public Osoby()
        {
            InitializeComponent();
        }

        private void insertName_Click(object sender, EventArgs e)
        {
            string enteredName = nameTextBox.Text;

            if (String.IsNullOrEmpty(enteredName))
            {
                MessageBox.Show("Zadejte jm�no osoby");
            }

            nameListBox.Items.Add(enteredName);
        }

        private void insertLastname_Click(object sender, EventArgs e)
        {
            string enteredLastame = lastnameTextBox.Text;

            if (String.IsNullOrEmpty(enteredLastame))
            {
                MessageBox.Show("Zadejte p��jmen� osoby");
            }

            lastnameListBox.Items.Add(enteredLastame);
        }

        private void konecToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Opravdu chcete program ukon�it?", "Potvrzen�", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void jm�noToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string?[] lines = new string[nameListBox.Items.Count];

            for (int i = 0; i < nameListBox.Items.Count; i++)
            {
                lines[i] = nameListBox.Items[i].ToString();
            }

            saveToFile(lines);
        }

        private void p��jmen�ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string?[] lines = new string[lastnameListBox.Items.Count];

            for (int i = 0; i < lastnameListBox.Items.Count; i++)
            {
                lines[i] = lastnameListBox.Items[i].ToString();
            }

            saveToFile(lines);
        }

        private void jm�noP��jmen�ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lastnameListBox.Items.Count != nameListBox.Items.Count)
            {
                MessageBox.Show("Rozd�ln� po�et jmen a p��jmen�");
                return;
            }

            string[] lines = new string[lastnameListBox.Items.Count];

            for (int i = 0; i < lastnameListBox.Items.Count; i++)
            {
                lines[i] = lastnameListBox.Items[i].ToString() + " " + lastnameListBox.Items[i].ToString();
            }

            saveToFile(lines);
        }

        private void jm�noP��jmen�ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            string[] names = readFile();

            for (int i = 0; i < names.Length; i++) 
            { 
                string[] name = names[i].Split(" ");
                if (name.Length == 2)
                {
                    nameListBox.Items.Add(name[0]);
                    lastnameListBox.Items.Add(name[1]);
                }

                else 
                {
                    MessageBox.Show("Neplatn� form�t jm�na");
                    return;
                }
            }
        }

        private void saveToFile(string?[] textLines)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            saveFileDialog.Filter = "Textov� soubor (*.txt)|*.txt";
            saveFileDialog.FilterIndex = 2;
            saveFileDialog.RestoreDirectory = true;

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                string filename = saveFileDialog.FileName;

                StreamWriter sw = new StreamWriter(filename);
                for (int i = 0; i < textLines.Length; i++)
                {
                    sw.WriteLine(textLines[i]);
                }
                sw.Close();
            }
        }

        private string[] readFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Textov� soubor|*.txt";
            openFileDialog.FilterIndex = 2;
            openFileDialog.RestoreDirectory = true;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                String filePath = openFileDialog.FileName;
                return File.ReadAllLines(filePath);
            }

            return new string[0];
        }
    }
}