# Návrh uživatelských rozhraní

### Rozdělení cvičení
- 1-7 - Web
- 8-11 - Windows forms C#
- Závěrečné práce

### Použití

```git
git@gitlab.com:jktech-vspj/nur.git
```

**Pro CV5 je potřeba přidat video do složky CV5/Videos/video.mp4 a audio do CV5/Audio/audio.mp3**, můžete použít jakékoliv, a zdrojové soubory upravit v html.

### Popis
Vypracované úkoly z předmětu *Návrh uživatelských rozhraní* na VŠPJ. Pro cvičení 6 a závěrečnou práci je potřeba sass compiler. Jakou závěrečnou práci jsem vypracoval web kapely tool, a WF aplikaci přehrávač hudby.

### Nástroje
- [Sass Compiler pro VS-Code](https://marketplace.visualstudio.com/items?itemName=ritwickdey.live-sass)
- [Visual Studio](https://visualstudio.microsoft.com/)